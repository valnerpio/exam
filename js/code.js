/******DATE PICKER******/
$(function() {
	$("#date").datepicker();		
});
/****** END DATE PICKER*****/


$(document).ready(function(){

	/*******AJAX CODE / FUNCTIONS ADD, DELETE, EDIT & RETRIEVE DATA *****/	

	lastID = function(){
	  $.ajax({
		type:"post",
		url:"process.php",
		data:"action=lastid",
		success:function(data){
			 return data;
		}
	  });
	}	
	
	loadData = function(){
	  $.ajax({
		type:"post",
		url:"process.php",
		data:"action=load",
		success:function(data){
			 $('#dataoutput').html(data);
		}
	  });
	}
	
	saveData = function(id){
		var name = $("#name_"+id).val();
		var message = $("#message_"+id).val();
		var date = $("#date_"+id).val();

		$.ajax({
			type:"post",
			url:"process.php",
			data:"name="+name+"&message="+message+"&date="+date+"&action=savedata",
			success:function(data){
				loadData();				  
			}
		});			
	}
	
	editData = function(id){
		var name = $("#name_"+id).val();
		var message = $("#message_"+id).val();
		var date = $("#date_"+id).val();
		
		$.ajax({
			type:"post",
			url:"process.php",
			data:"name="+name+"&id="+id+"&message="+message+"&date="+date+"&action=editdata",
			success:function(data){
				console.log("data edited");
			}
		});			
	}
	
	deleteData = function(id){		
		$.ajax({
			type:"post",
			url:"process.php",
			data:"id="+id+"&action=deletedata",
			success:function(data){
				console.log("data deleted");
			}
		});			
	}
	
	loadData(); //load data from database
	
	
	/*******ADD, EDIT, DELETE DATA *****/
	var id = lastID();			
	
	$("#addrecord").click(function(){				
		id++;
		var table = $(".table").find('tbody');	//find table				
		var name = $("#name").val();  
		var message = $("#message").val(); 				
		var date = $("#date").val(); 
		var edit = '<a id="'+id+ '" class="editIcon"><img class="edit" src="images/edit.png" title="EDIT"/></a>';
		var del = '<a id="'+id+ '" class="deleteIcon"><img class="del" src="images/delete.png" title="DELETE"/></a>';
		var save = '<a id="'+id+ '" class="saveIcon"><img class="save" src="images/save.png" title="SAVE"/></a>';
		var cancel = '<a id="'+id+ '" class="cancelIcon"><img class="cancel" src="images/cancel.png" title="CANCEL"/></a>';
		var data = $('<tr class="tr_data_'+id+'"><td id="name_td_'+id+'">' +name+  '</td><td id="message_td_'+id+'">' +message+'</td><td id="date_td_'+id+'">'+date+'</td><td>'+edit+del+'</td></tr>');	 //set the data
		var editdata = $('<tr style="display:none" class="tr_edit_'+id+'"><td><input class="form-control" name="name" id="name_'+id+'" type="text" value="'+name+'"></td><td><textarea name="message" style="resize:none" class="form-control" rows="5" id="message_'+id+'">'+message+'</textarea></td><td><input class="editdate form-control" name="date" id="date_'+id+'" type="text" value="'+date+'"></td><td>'+save+cancel+'</td></tr>');	 //hidden input type text data
				
		table.append(data); //append the data
		table.append(editdata); //append the data
		
		saveData(id);
		
	});
});



/****** EDIT DATA *****/
$(document).on('click', '.editIcon', function() {	
	$(".editdate").datepicker();		
	var id=$(this).attr('id');	
	$(".tr_data_"+id).hide();
	$(".tr_edit_"+id).show();			
	$("#name_"+id).focus();		
});

/****** SAVE DATA *****/
$(document).on('click', '.saveIcon', function() {		
	var id=$(this).attr('id');			
	/***CHECK IF BLANK***/
	if( ($("#name_"+id).val()=="") ){
		alert("Please enter data, dont leave blank.");			
		$("#name_"+id).focus();
		return false;
	}
	else if( ($("#message_"+id).val()=="") ){
		alert("Please enter data, dont leave blank.");			
		$("#message_"+id).focus();
		return false;
	}
	else if( ($("#date_"+id).val()=="") ){
		alert("Please enter data, dont leave blank.");			
		$("#date_"+id).focus();
		return false;
	}
	else{
		var name = $("#name_"+id).val();  
		var message = $("#message_"+id).val(); 
		var date = $("#date_"+id).val();	
		$("#name_"+id).attr('value',name);
		$("#message_"+id).attr('value',message);
		$("#date_"+id).attr('value',date);
		//SET NEW VALUE DATA
		$("#name_td_"+id).text(name);	
		$("#message_td_"+id).text(message);
		$("#date_td_"+id).text(date);
		$(".tr_edit_"+id).hide();
		$(".tr_data_"+id).show();
		editData(id);
	}	
});

/****** CANCEL EDIT DATA *****/
$(document).on('click', '.cancelIcon', function() {		
	var id=$(this).attr('id');
	var name = $("#name_td_"+id).text();  	
	var message = $("#message_td_"+id).text(); 
	var date = $("#date_td_"+id).text();	
	//RETURN ORIGINAL VALUE DATA
	$("#name_"+id).val(name);	
	$("#message_"+id).val(message);
	$("#date_"+id).val(date);
	$(".tr_edit_"+id).hide();
	$(".tr_data_"+id).show();	
});

/****** DELETE DATA *****/
$(document).on('click', '.deleteIcon', function() {
	var id=$(this).attr('id');	

	if(confirm("Are you sure you want to delete this data?")){        
		//$(".tr_"+tr_id).hide();
		$(".tr_data_"+id).remove();
		deleteData(id);
    }
    else{
        return false;
    }

});

