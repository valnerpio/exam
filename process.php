<?php
  
	$con=mysqli_connect("localhost","root","","exam"); //server, username, pass, database name

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	
	$action = $_POST["action"];
	$id = mysqli_real_escape_string($con,$_POST["id"]);	
	$name = mysqli_real_escape_string($con,$_POST["name"]);
	$message = mysqli_real_escape_string($con,$_POST["message"]);
	$date = mysqli_real_escape_string($con,$_POST["date"]);
	$date = date('Y-m-d', strtotime($date));	

	if($action == "lastid"){		
		$sql = "SELECT id FROM table_exam ORDER BY id DESC LIMIT 1";
		$lastid = implode(mysqli_fetch_assoc(mysqli_query($con,$sql)));		
		echo $lastid;
	}
	
	elseif($action == "load"){						
			$sql="SELECT * FROM table_exam";
			$result=mysqli_query($con,$sql);
			
			while($data = mysqli_fetch_array($result))
			{  
				$edit = '<a id="'.$data["id"].'" class="editIcon"><img class="edit" src="images/edit.png" title="EDIT"/></a>';
				$del = '<a id="'.$data["id"].'" class="deleteIcon"><img class="del" src="images/delete.png" title="DELETE"/></a>';
				$save = '<a id="'.$data["id"].'" class="saveIcon"><img class="save" src="images/save.png" title="SAVE"/></a>';
				$cancel = '<a id="'.$data["id"].'" class="cancelIcon"><img class="cancel" src="images/cancel.png" title="CANCEL"/></a>';
				
				//real data
				echo ('<tr class="tr_data_'.$data["id"].'"><td id="name_td_'.$data["id"].'">' .$data["name"].  '</td><td id="message_td_'.$data["id"].'">' .$data["message"].'</td><td id="date_td_'.$data["id"].'">'.$data["mydate"].'</td><td>'.$edit.$del.'</td></tr>');	 //set the data
				
				//hidden input type text data - for editing
				echo ('<tr style="display:none" class="tr_edit_'.$data["id"].'"><td><input class="form-control" name="name" id="name_'.$data["id"].'" type="text" value="'.$data["name"].'"></td><td><textarea name="message" style="resize:none" class="form-control" rows="5" id="message_'.$data["id"].'">'.$data["message"].'</textarea></td><td><input class="editdate form-control" name="date" id="date_'.$data["id"].'" type="text" value="'.$data["mydate"].'"></td><td>'.$save.$cancel.'</td></tr>');	 
			}
	}
	
	elseif($action == "savedata"){						
			$query = mysqli_query($con, "INSERT INTO table_exam(name,message,mydate) values('$name','$message','$date') ");		
			$last_id = mysqli_insert_id($con);
			if($query){
				echo $last_id;
			}
	}
	
	elseif($action == "editdata"){
			$sql = "UPDATE table_exam SET name='$name',message='$message',mydate='$date' WHERE id=$id";

			if (mysqli_query($con, $sql)) {
			echo "Record updated successfully";
			} else {
			echo "Error updating record: " . mysqli_error($con);
			}
	}
	
	elseif($action == "deletedata"){
			$sql = "DELETE FROM table_exam WHERE id=$id";

			if (mysqli_query($con, $sql)) {
				echo "Record deleted successfully";
			} else {
				echo "Error deleting record: " . mysqli_error($con);
			}
	}
	
	mysqli_close($con);
?>