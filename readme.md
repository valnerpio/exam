#Exam for PHP Junior Developer - Val Nerpio

Create a page that have a table with the following columns:

- Name
- Message
- Date

The page should be able to:

- Add, edit, and delete records
- Adding record via popup modal form
- Record add, update, and delete should show without refreshing the page

Use the following:

- Twitter bootstrap (Layout and Modal form)
- JQuery for AJAX

Upload your code to a GIT repository that is accessible to public and email me the instruction on how to download it and deploy it locally. I prefer BitBucket.

Note: don't use a php Framework, do this natively.